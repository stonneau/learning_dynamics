from centroidal_dynamics import *
from numpy import array, asmatrix, matrix, newaxis, zeros
import numpy as np
import cPickle as pickle
import sys
sys.path.append('../../synth')
sys.path.append('../../nn')
sys.path.append('../../motion')

from qp_giwc import min_acc_error

#loading bounds
       
res = None #{ "com" : com, "ddc" : ddc,  "p" : p}
with open("variables_bounds.pickle", 'r') as handle:
    res = pickle.load(handle)
    handle.close()    

com = res["com"]        
ddc = res["ddc"]        
p   = res["p"] 
    
#~ range_min_nu = 0.01;
range_min_nu = 0.9;
range_max_nu = 1.2;
n_contacts = 4


# start easy, just take 10000 samples
num_samples = 10000; outfile = "t10000"
#~ num_samples = 100000; outfile = "t100000"
#~ data = array(num_samples, n_contacts * 3 + 6 + 3)

contactPositions = np.random.uniform(low=p[0], high=p[1], size =(num_samples * (n_contacts-1), 3)).reshape([num_samples, (n_contacts-1)*3])
contactActivations = np.random.randint(0, 2,(num_samples,4))

comPositions = np.random.uniform(low=com[0], high=com[1], size =(num_samples, 3))
ddcPositions = np.random.uniform(low=ddc[0], high=ddc[1], size =(num_samples, 3))
nus = np.random.uniform(low=range_min_nu, high=range_max_nu, size=(num_samples,))
X = x = np.hstack((contactPositions,contactActivations,comPositions,ddcPositions,nus[:,np.newaxis]))
Y = zeros((num_samples,1))

cpIdx = 0
cActIdx = cpIdx +contactPositions.shape[1]
cPosIdx = cActIdx+ contactActivations.shape[1]
ddcPosIdx = cPosIdx+ comPositions.shape[1]
nusIdx = ddcPosIdx+ ddcPositions.shape[1]

#~ from giwc import robustness, squareContact
from giwc import robustness

def squareContact(center, normal,side):
    rg = [-side,side]    
    return [[center[0]+rgi,center[1],center[2]+rgj] for rgi in rg for rgj in rg]

z = array([0.,0.,1.])

num_success = 0
num_lp_failed = 0

lengths=np.array([
                2.40,7.15,7.49,2.36,2.37,7.43,7.50,2.41,
                2.04,2.05,1.75,1.76,2.90,4.98,3.48,0.71,
                2.73,5.24,3.44,0.62]);
masstot = sum(lengths)

def computeRobustness(x):
    cpoints = [x[i*3:i*3+3] for i in range(n_contacts-1) if x[cActIdx+i+1] > 0.5 ]
    if x[cActIdx] == 1:
        cpoints = [[0.,0.,0.]] + cpoints
    if(len(cpoints) == 0):
        #~ print "no contact", np.linalg.norm(x[ddcPosIdx:nusIdx])
        return np.linalg.norm(x[ddcPosIdx:nusIdx])
    cpoints = [squareContact(cp, z, 0.1)  for cp in cpoints]    
    cpoints = [el for sublist in cpoints for el in sublist]
    P = array(cpoints)
    N = array([z for _ in range(len(cpoints))])
    mass = masstot
    c = x[cPosIdx:ddcPosIdx]
    ddc = x[ddcPosIdx:nusIdx]
    nu = x[-1]
    rob = min_acc_error(P, N, mass, nu, c, ddc, g=array([0,0,-9.81]), dL =array([0,0,0]))
    if(rob <= 0.1):
        global num_success
        num_success +=1
    return rob
    
import random


def process(idx):
    try:
        Y[idx] = computeRobustness(X[idx])
    except ValueError:
        print "lp failed"
        # retry with proparbility_ 0.8
        restart = random.uniform(0, 1)
        if restart > 1:
            global num_lp_failed
            Y[idx] = 10e15
            num_lp_failed += 1
        else:
            X[idx,:-1] = np.random.uniform(low=range_min, high=range_max, size=(1,n_contacts * 3 + 6))
            X[idx,-1] = np.random.uniform(low=range_min_nu, high=range_max_nu)
            process(idx)

for i in range(num_samples):
    process(i)
        
    

Xmean = X.mean(axis=0)
Ymean = Y.mean()

Xstd = X.std(axis=0)
Ystd = Y.std()

#~ Xnorm = (X  - Xmean) / Xstd
#~ Ynorm = (Y  - Ymean) / Ystd

res = { "X" : X, "Y" : Y,  "Xmean" : Xmean, "Ymean" : Ymean,
        "Xstd" : Xstd, "Ystd" : Ystd}
        
print "num sucessful samples", num_success
print "num num_lp_failed", num_lp_failed
        
with open(outfile, 'wb') as handle:
    pickle.dump(res, handle, protocol=pickle.HIGHEST_PROTOCOL)
