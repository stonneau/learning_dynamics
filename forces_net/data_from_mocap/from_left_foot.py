from centroidal_dynamics import *
from numpy import array, asmatrix, matrix, newaxis, zeros
import numpy as np
import cPickle as pickle
import sys
sys.path.append('../../synth')

import scipy.io as io
import theano
import theano.tensor as T

sys.path.append('../../nn')
sys.path.append('../../motion')

from network import create_core
rng = np.random.RandomState(23455)

#X = np.load('../data/processed/data_edin_locomotion.npz')['clips']
#X = np.load('../data/processed/data_hdm05.npz')['clips']
X = np.load('../../data/processed/data_cmu.npz')['clips']
X = np.swapaxes(X, 1, 2).astype(theano.config.floatX)

preprocess = np.load('../../synth/preprocess_core.npz')
X = (X - preprocess['Xmean']) / preprocess['Xstd']

batchsize = 1
window = X.shape[2]

X = theano.shared(X, borrow=True)

network = create_core(batchsize=batchsize, window=window, dropout=0.0, depooler=lambda x,**kw: x/2)
network.load(np.load('network_core.npz'))

from giwc import computeDynamicsDataPerFrame

m = [np.array([200,200,200]),np.array([-200,-200,-200])]
com = m[:]
ddc = m[:]
p = m[:]
range_min_nu = 0.01;
range_max_nu = 1.1;
inc = 0.2
n_contacts = 4


def up_bd(var, current):
    var[0] = [min(var[0][i], current[i]) for i in range(current.shape[0])]
    var[1] = [max(var[1][i], current[i]) for i in range(current.shape[0])]
    return var
        

def zero_in_left_foot(dyn_data):
    offset = dyn_data[0][0]
    dyn_zero = [el - offset for el in dyn_data[0]]
    dyn_data_c = dyn_data[1]-offset
    dyn_data_ddc = dyn_data[2]-offset
    global com, ddc, p
    com = up_bd(com, dyn_data_c)
    ddc = up_bd(ddc, dyn_data_ddc)
    p   = [up_bd(  p, el) for el in dyn_zero][-1]
    return [dyn_zero,dyn_data_c, dyn_data_ddc]

def update_bounds(dyn_data):
    return [zero_in_left_foot(el) for el in dyn_data]
    

num_iter = 10
index = 8539
res = None
    
for _ in range(num_iter):
    Xorgi = np.array(X[index:index+1].eval())
    Xorgi = (Xorgi * preprocess['Xstd']) + preprocess['Xmean']

    H = theano.shared(np.array(network[0](theano.shared((Xorgi - preprocess['Xmean']) / preprocess['Xstd'])).eval()))
    V = ((network[1](H) * preprocess['Xstd']) + preprocess['Xmean']).eval()
    feet = np.array([[12,13,14], [15,16,17],[24,25,26], [27,28,29]])
    res = computeDynamicsDataPerFrame(V, 0, V.shape[-1], feet, 54.)
    res = update_bounds(res)

    index = rng.randint(X.shape[0].eval())
    
res = { "com" : com, "ddc" : ddc,  "p" : p}
        
        
with open("variables_bounds.pickle", 'wb') as handle:
    pickle.dump(res, handle, protocol=pickle.HIGHEST_PROTOCOL)
    handle.close()
