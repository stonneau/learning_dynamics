import numpy
import pandas
from keras.models import Sequential
from keras.layers import Dense
from keras.wrappers.scikit_learn import KerasRegressor
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline

float_formatter = lambda x: "%.2f" % x
numpy.set_printoptions(formatter={'float_kind':float_formatter})

import cPickle as pickle
#~ from cPickle import load

#~ with open('./data/testdata.pickle', 'r') as handle:
with open('./data_from_mocap/t10000', 'r') as handle:
    data = pickle.load(handle)
    handle.close()

#~ res = { "X" : X, "Y" : Y,  "Xmean" : Xmean, "Ymean" : Ymean,
        #~ "Xstd" : Xstd, "Ystd" : Ystd}

X = data["X"]
Y = data["Y"]

Xmean = data["Xmean"]
Ymean = data["Ymean"]

Xstd = data["Xstd"]
Ystd = data["Ystd"]

Xnorm = (X  - Xmean) / Xstd
Ynorm = (Y  - Ymean) / Ystd

#~ import numpy as np
#~ seed = 7
#~ np.random.seed(seed)

def baseline_model():
	# create model
	model = Sequential()
	model.add(Dense(20, input_dim=20, kernel_initializer='normal', activation='relu'))
	model.add(Dense(1, kernel_initializer='normal'))
	# Compile model
	model.compile(loss='mean_squared_error', optimizer='adam')
	return model

# define the model
def larger_model():
	# create model
	model = Sequential()
	model.add(Dense(20, input_dim=20, kernel_initializer='normal', activation='relu'))
	#~ model.add(Dense(13, kernel_initializer='normal', activation='relu'))
	#~ model.add(Dense(15, kernel_initializer='normal', activation='relu'))
	#~ model.add(Dense(55, kernel_initializer='normal', activation='relu'))
	#~ model.add(Dense(155, kernel_initializer='normal', activation='relu'))
	model.add(Dense(6, kernel_initializer='normal', activation='relu'))
	model.add(Dense(1, kernel_initializer='normal'))
	# Compile model
	model.compile(loss='mean_squared_error', optimizer='adam')
	return model

def wider_model():
	# create model
	model = Sequential()
	model.add(Dense(20, input_dim=20, kernel_initializer='normal', activation='relu'))
	model.add(Dense(1, kernel_initializer='normal'))
	# Compile model
	model.compile(loss='mean_squared_error', optimizer='adam')
	return model

# fix random seed for reproducibility
seed = 7
numpy.random.seed(seed)
# evaluate model with standardized dataset
#~ estimator = KerasRegressor(model)

#~ kfold = KFold(n_splits=10, random_state=seed)
#~ results = cross_val_score(estimator, X, Y, cv=kfold)
#~ print("Results: %.2f (%.2f) MSE" % (results.mean(), results.std()))

model = baseline_model()
lmodel = larger_model()
wmodel = wider_model()
model.fit(Xnorm, Ynorm, epochs=200 , batch_size=5,  verbose=1, validation_split=0.3)
#~ lmodel.fit(Xnorm, Ynorm, epochs=100, batch_size=5,  verbose=1, validation_split=0.3)
lmodel.fit(Xnorm, Ynorm, epochs=100, batch_size=10,  verbose=1, validation_split=0.3)
#~ lmodel.fit(Xnorm, Ynorm, epochs=100, batch_size=1,  verbose=1, validation_split=0.3)
#~ lmodel.fit(Xnorm, Ynorm, epochs=100, batch_size=20,  verbose=1, validation_split=0.3)
#~ wmodel.fit(Xnorm, Ynorm, epochs=200, batch_size=5,  verbose=1, validation_split=0.3)

def pred(mod, datain, dataout):
    x  = (datain   - Xmean) / Xstd
    y  = (dataout  - Ymean) / Ystd
    ny = mod.predict(x.reshape((1,20)))
    #~ print "expected, got ", y, " ", ny
    print "expected, got ", dataout, " ", ny * Ystd  + Ymean

    
#~ a = [ pred(model, X[i],Y[i]) for i in range(10)]
a = [ pred(lmodel, X[i],Y[i]) for i in range(10)]
#~ a = [ pred(wmodel, X[i],Y[i]) for i in range(10)]
