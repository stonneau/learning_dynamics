import sys
import time
import numpy as np
import scipy.io as io
import theano
import theano.tensor as T

from giwc import computeRobustnessPerFrame

def multiconstraint(*fs): 
    return lambda H, V: (sum(map(lambda f: f(H, V), fs)) / len(fs))

def trajectory(traj):
    
    def trajectory_constraint(H, V):
        velocity_scale = 10
        return velocity_scale * T.mean((V[:,-7:-4] - traj)**2)
    
    return trajectory_constraint
    
def foot_sliding(labels):
    
    def foot_sliding_constraint(H, V):
        
        feet = np.array([[12,13,14], [15,16,17],[24,25,26], [27,28,29]])
        contact = (labels > 0.5).astype(theano.config.floatX)
        
        offsets = T.concatenate([
            V[:,feet[:,0:1]],
            T.basic.zeros((V.shape[0],len(feet),1,V.shape[2])),
            V[:,feet[:,2:3]]], axis=2)
        
        def cross(A, B):
            return T.concatenate([
                A[:,:,1:2]*B[:,:,2:3] - A[:,:,2:3]*B[:,:,1:2],
                A[:,:,2:3]*B[:,:,0:1] - A[:,:,0:1]*B[:,:,2:3],
                A[:,:,0:1]*B[:,:,1:2] - A[:,:,1:2]*B[:,:,0:1]
            ], axis=2)
        
        rotation = -V[:,-5].dimshuffle(0,'x','x',1) * cross(np.array([[[0,1,0]]]), offsets)
        
        velocity_scale = 10
        cost_feet_x = velocity_scale * T.mean(contact[:,:,:-1] * (((V[:,feet[:,0],1:] - V[:,feet[:,0],:-1]) + V[:,-7,:-1].dimshuffle(0,'x',1) + rotation[:,:,0,:-1])**2))
        cost_feet_z = velocity_scale * T.mean(contact[:,:,:-1] * (((V[:,feet[:,2],1:] - V[:,feet[:,2],:-1]) + V[:,-6,:-1].dimshuffle(0,'x',1) + rotation[:,:,2,:-1])**2))
        #cost_feet_y = T.mean(contact * ((V[:,feet[:,1]] - np.array([[0.75], [0.0], [0.75], [0.0]]))**2))
        cost_feet_y = velocity_scale * T.mean(contact[:,:,:-1] * ((V[:,feet[:,1],1:] - V[:,feet[:,1],:-1]) **2))
        cost_feet_h = 10.0 * T.mean(T.minimum(V[:,feet[:,1],1:], 0.0)**2)
        return (cost_feet_x + cost_feet_z + cost_feet_y + cost_feet_h) / 4
    
    return foot_sliding_constraint

def dynamics(labels,nu = 0.5):
    def dynamic_constraint(Vwhole, frameId, window_size):
        feet = np.array([[12,13,14], [15,16,17],[24,25,26], [27,28,29]])
        contact = (labels > 0.5)
                
        lengths=np.array([
                2.40,7.15,7.49,2.36,2.37,7.43,7.50,2.41,
                2.04,2.05,1.75,1.76,2.90,4.98,3.48,0.71,
                2.73,5.24,3.44,0.62]);
        masstot = sum(lengths)
        return computeRobustnessPerFrame(contact, Vwhole, frameId, window_size, feet, masstot, nu = nu)
    return dynamic_constraint
    

def joint_lengths(
    parents=np.array([-1,0,1,2,3,4,1,6,7,8,1,10,11,12,12,14,15,16,12,18,19,20]),
    lengths=np.array([
            2.40,7.15,7.49,2.36,2.37,7.43,7.50,2.41,
            2.04,2.05,1.75,1.76,2.90,4.98,3.48,0.71,
            2.73,5.24,3.44,0.62], dtype=theano.config.floatX)):

    def joint_lengths_constraint(H, V):
        
        J = V[:,:-7].reshape((V.shape[0], len(parents), 3, V.shape[2]))        
        return T.mean((
            T.sqrt(T.sum((J[:,2:] - J[:,parents[2:]])**2, axis=2)) - 
            lengths[np.newaxis,...,np.newaxis])**2)

    return joint_lengths_constraint
        
    
def constrain(X, forward, backward, preprocess, constraint, alpha=0.1, iterations=100):
    
    H = theano.shared(np.array(forward(theano.shared((X - preprocess['Xmean']) / preprocess['Xstd'])).eval()))
    
    V = (backward(H) * preprocess['Xstd']) + preprocess['Xmean']
    
    cost = constraint(H, V)
        
    self_alpha = alpha
    self_beta1 = 0.9
    self_beta2 = 0.999
    self_eps = 1e-05
    self_batchsize = 1

    self_params = [H]
    self_m0params = [theano.shared(np.zeros(p.shape.eval(), dtype=theano.config.floatX)) for p in self_params]
    self_m1params = [theano.shared(np.zeros(p.shape.eval(), dtype=theano.config.floatX)) for p in self_params]
    self_t = theano.shared(np.array([1], dtype=theano.config.floatX))

    gparams = T.grad(cost, self_params)
    m0params = [self_beta1 * m0p + (1-self_beta1) *  gp     for m0p, gp in zip(self_m0params, gparams)]
    m1params = [self_beta2 * m1p + (1-self_beta2) * (gp*gp) for m1p, gp in zip(self_m1params, gparams)]
    params = [p - (self_alpha / self_batchsize) * 
              ((m0p/(1-(self_beta1**self_t[0]))) /
        (T.sqrt(m1p/(1-(self_beta2**self_t[0]))) + self_eps))
        for p, m0p, m1p in zip(self_params, m0params, m1params)]

    updates = ([( p,  pn) for  p,  pn in zip(self_params, params)] +
               [(m0, m0n) for m0, m0n in zip(self_m0params, m0params)] +
               [(m1, m1n) for m1, m1n in zip(self_m1params, m1params)] +
               [(self_t, self_t+1)])

    constraint_func = theano.function([], cost, updates=updates)

    start = time.clock()
    for i in range(iterations):
       cost = constraint_func()
       print('Constraint Iteration %i, error %f' % (i, cost))
    print('Constraint: %0.4f' % (time.clock() - start))
    
    return (np.array(backward(H).eval()) * preprocess['Xstd']) + preprocess['Xmean']
    
from scipy.optimize import approx_fprime as approx_fprime
eps = np.sqrt(np.finfo(float).eps)
    
def dfOne(Vwhole, frameId, currentFrameId, currentWindowSize, f, epsilon,grad,ei):
    start_impact = max(frameId, currentFrameId-2)
    end_impact = min(currentWindowSize+currentFrameId, currentFrameId+2)-currentFrameId
    Vshape = [Vwhole.shape[0],Vwhole.shape[1], end_impact]
    f0 = f(Vwhole,start_impact, end_impact) 
    idx = None
    for i in range(Vshape[1]):
        #~ print "index", idx
        idx = (0,i,currentFrameId)
        ei[idx]+=epsilon
        c = f(Vwhole+ei,start_impact, end_impact)
        grad[idx] = (c - f0) / epsilon
        ei[idx]-=epsilon
        
#this is actually slower ?
def dfold(Vwhole, frameId, window_size, f, epsilon):
    f0 = f(Vwhole,frameId, window_size)
    print "f0", f0
    V = Vwhole[:,:,frameId:frameId+window_size]
    ei   = np.zeros(Vwhole.shape, float)
    Vshape = V.shape
    grad = np.zeros(Vshape, float)    
    [dfOne(Vwhole, frameId, frameId+j, window_size-j, f, epsilon,grad,ei)  for j in range(window_size)]
    return grad 
    
def df(Vwhole, frameId, window_size, f, epsilon):
    f0 = f(Vwhole,frameId, window_size)
    print "f0", f0
    V = Vwhole[:,:,frameId:frameId+window_size]
    Vshape = V.shape
    grad = np.zeros(Vshape, float)
    ei   = np.zeros(Vwhole.shape, float)
    it = np.nditer(V, flags=['multi_index'])
    out = 0
    init = 0
    indices = [(i,j,k) for i in range(Vshape[0]) for j in range(Vshape[1]) for k in range(Vshape[2])]
    while not it.finished:
        ei[it.multi_index]+=epsilon
        c = f(Vwhole+ei,frameId, window_size)
        grad[it.multi_index] = (c - f0) / epsilon
        ei[it.multi_index]-=epsilon
        it.iternext()
        
    return grad 
        
import cPickle as pickle    

#~ import gc
def constrain_manual(X, forward, backward, preprocess, constraint, alpha=0.01, iterations=100, H = None):
    if H == None:
        H = theano.shared(np.array(forward(theano.shared((X - preprocess['Xmean']) / preprocess['Xstd'])).eval()))  
    
    eps = np.sqrt(np.finfo(float).eps)
    start = 0
    def cost_fun(Vwhole, frameId, window_size):
        c =  constraint(Vwhole, frameId, window_size)
        return c
    
    def grad_fun(Hv, H_range = [0,1], V_range = None):
        V = (backward(Hv) * preprocess['Xstd']) + preprocess['Xmean']        
        VWhole = V.copy()        
        
        #compute the total influence on the output V
        impact_frameId = max(0, H_range[0]*2-12)
        impact_windowsize = min(V.shape[-1].eval(), H_range[1]*2+13);
        print "impact_frameId", impact_frameId
        print "impact_windowsize", impact_windowsize
        
        if V_range != None:
            assert(V_range[0]>=impact_frameId)
            assert(V_range[1]<=impact_frameId+impact_windowsize)
            impact_frameId =V_range[0]
            impact_windowsize = V_range[1]-impact_frameId
            
        
        #now compute the number of Vs fully impacted by this
        H_grad = np.zeros((impact_windowsize*V.shape.eval()[1],H_range[1]-H_range[0],Hv.shape.eval()[1]))
        H_offset = H_range[0]
        start = time.clock()        
        gradCost  = df(VWhole.eval(), impact_frameId, impact_windowsize, cost_fun,eps)  
        print('Cost Gradient time: %0.4f' % (time.clock() - start))
        
        
        start = time.clock()     
        grad = T.jacobian(V[:,:,impact_frameId:impact_frameId+impact_windowsize].flatten(), [Hv])[0][:,:,:,H_range[0]:H_range[1]]
        print('Cost grad comp time: %0.4f' % (time.clock() - start))
        
        
        start = time.clock()  
        res= grad.T.dot(T.constant(gradCost.flatten())).eval()
        print('Cost grad eval time: %0.4f' % (time.clock() - start))
        return res
        
        #~ for frameId in range(H_range[0],H_range[1]):
            #~ grad = T.jacobian(V[:,:,impact_frameId:impact_frameId+impact_windowsize].flatten(), [Hv])[0][:,:,:,frameId].eval()
            #~ H_grad[:,frameId-H_offset:frameId-H_offset+1,:] = grad
            #~ print "grad for col", frameId
        #~ return H_grad.T.dot(gradCost.flatten())
        #~ return gradCost

    
    Hvar = np.zeros(H.shape.eval())
    #~ H_range = [60,66];
    #~ V_range = [110,135]
    H_range = [0,6];
    V_range = [0,25]
    print "grad space spae", Hvar.shape
    print "grad space spae", Hvar[:,:,H_range[0]:H_range[1]].shape
    for i in range(iterations):       
        start = time.clock()
        grad = grad_fun(H+T.constant(Hvar, name='Hvar'), H_range = H_range, V_range = V_range)
        print('Gradient time: %0.4f' % (time.clock() - start))
        print "iteration ", i
        print "grad spae", grad.shape
        Hvar[:,:,H_range[0]:H_range[1]] -= alpha * grad.T  
        with open("H_6_25_50"+str(i), 'wb') as handle:
            pickle.dump(H.eval()+Hvar, handle, protocol=pickle.HIGHEST_PROTOCOL)  
            handle.close()        
    return (np.array(backward(H+T.constant(Hvar, name='Hvar')).eval()) * preprocess['Xstd']) + preprocess['Xmean']
    #~ return Vvar.reshape(shapeV)
     
     
def fpar(V,H,hcol):
    def res(frameId):
        return T.jacobian(V[:,:,frameId:frameId+1].flatten(), [H])[0][:,:,:,hcol].eval()
    return res
        
def test_gradient_affect_range(X, forward, backward, preprocess, constraint, alpha=0.01, iterations=100, hcol = 26):
    H = theano.shared(np.array(forward(theano.shared((X - preprocess['Xmean']) / preprocess['Xstd'])).eval()))    
    V = (backward(H) * preprocess['Xstd']) + preprocess['Xmean'] 
    window_size = 1
    fid = hcol * 2
    for frameId in [fid+12,fid+13,fid+14,fid-12,fid-13,fid-14]:
        grad = T.jacobian(V[:,:,frameId:frameId+window_size].flatten(), [H])[0][:,:,:,hcol].eval()
        if np.linalg.norm(grad)> 0 :
            print "frame i concerned by col h", frameId, " ", hcol
        else:
            print "frame i NOT concerned by col h", frameId, " ", hcol
    return grad
    # this probably means I need to update 12 frames at a time to compute the derivative 
