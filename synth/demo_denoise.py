import sys
import numpy as np
import scipy.io as io
import theano
import theano.tensor as T

sys.path.append('../nn')

from network import create_core
from constraints import constrain, foot_sliding, joint_lengths, trajectory, multiconstraint, constrain_manual,  dynamics, test_gradient_affect_range

rng = np.random.RandomState(23455)

#X = np.load('../data/processed/data_edin_locomotion.npz')['clips']
#X = np.load('../data/processed/data_hdm05.npz')['clips']
X = np.load('../data/processed/data_cmu.npz')['clips']
X = np.swapaxes(X, 1, 2).astype(theano.config.floatX)

preprocess = np.load('preprocess_core.npz')
X = (X - preprocess['Xmean']) / preprocess['Xstd']

batchsize = 1
window = X.shape[2]

X = theano.shared(X, borrow=True)

network = create_core(batchsize=batchsize, window=window, dropout=0.0, depooler=lambda x,**kw: x/2)
network.load(np.load('network_core.npz'))

from AnimationPlot import animation_plot

# 2021
# 13283

import cPickle as pickle    

for _ in range(1):

    index = rng.randint(X.shape[0].eval())
    index = 8539
    print(index)
    Xorgi = np.array(X[index:index+1].eval())
    Xnois = ((Xorgi * rng.binomial(size=Xorgi.shape, n=1, p=0.5)) / 0.5).astype(theano.config.floatX)
    #~ Xrecn = np.array(network(Xnois).eval())    
    Xrecn = np.array(X[index:index+1].eval())    

    Xorgi = (Xorgi * preprocess['Xstd']) + preprocess['Xmean']
    Xnois = (Xnois * preprocess['Xstd']) + preprocess['Xmean']
    Xrecn = (Xrecn * preprocess['Xstd']) + preprocess['Xmean']

    #~ animation_plot([Xnois, Xorgi], interval=15.15)

    #~ Xrecn = constrain(Xrecn, network[0], network[1], preprocess, multiconstraint(
        #~ foot_sliding(Xorgi[:,-4:].copy()),
        #~ joint_lengths(),
        #~ trajectory(Xorgi[:,-7:-4])), alpha=0.01, iterations=50)
        
    H = None
    with open("H_6_24_5", 'r') as handle:
        H = pickle.load(handle)  
        H = T.constant(H, name='H')
        handle.close()   
    window_size = 1
        
    Xrecn = constrain_manual(Xrecn, network[0], network[1], preprocess, dynamics(Xorgi[:,-4:].copy(),0.01),
    #~ Xrecn = test_gradient_affect_range(Xrecn, network[0], network[1], preprocess, dynamics(Xorgi[:,-4:].copy(),0.01),
        alpha=0.001, iterations=3, H=H)

    Xrecn[:,-7:-4] = Xorgi[:,-7:-4]
        
    animation_plot([Xnois, Xrecn, Xorgi], interval=1.15)
        
