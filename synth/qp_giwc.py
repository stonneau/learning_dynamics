from qp import solve_lp, quadprog_solve_qp

#~ (c, A_ub=None, b_ub=None, A_eq=None, b_eq=None, bounds=None, method='simplex', callback=None, options=None)[source]

    #~ Minimize a linear objective function subject to linear equality and inequality constraints.

    #~ Linear Programming is intended to solve the following problem form:

    #~ Minimize:     c^T * x

    #~ Subject to:   A_ub * x <= b_ub
                  #~ A_eq * x == b_eq

             

from numpy import array, vstack, zeros, sqrt, cross, identity, ones, asmatrix
from numpy.linalg import norm
import numpy as np

from math import cos, sin, tan, atan, pi
import matplotlib as mpl
import matplotlib.pyplot as plt
from pylab import axes
from mpl_toolkits.mplot3d import Axes3D


n = 3;      # generator size
cg = 4;     # number of generators per contact
USE_DIAGONAL_GENERATORS = True;
CONTACT_SET = 1;

def crossMatrix( v ):
        VP = np.array( [[  0,  -v[2], v[1] ],
                    [ v[2],  0,  -v[0] ],
                    [-v[1], v[0],  0   ]] );
        return VP;

## 
#  Given a list of contact points
#  as well as a list of associated normals
#  compute the gravito inertial wrench cone
#  \param p array of 3d contact positions
#  \param N array of 3d contact normals
#  \param simplify_cones if true inequality conversion will try to remove 
#  redundancies
#  \param params requires "mass" "g"  and "mu"
#  \return the CWC H, H w <= 0, where w is the wrench
def compute_generators(p, N, mass, g, mu):
        ''' compute generators '''
        c = p.shape[0];
        m = c*cg;            # number of generators
        S = np.zeros((n,m));
        T1 = np.zeros((c,n));
        T2 = np.zeros((c,n));
        muu = mu/sqrt(2);
        for i in range(c):
                ''' compute tangent directions '''
                N[i,:]  = N[i,:]/np.linalg.norm(N[i,:]);
                T1[i,:] = np.cross(N[i,:], [0,1,0]);
                if(np.linalg.norm(T1[i,:])<1e-5):
                        T1[i,:] = np.cross(N[i,:], [1,0,0]);
                T1[i,:] = T1[i,:]/np.linalg.norm(T1[i,:]);
                T2[i,:] = np.cross(N[i,:], T1[i,:]);
                T2[i,:] = T2[i,:]/np.linalg.norm(T2[i,:]);
                
                if(USE_DIAGONAL_GENERATORS):
                        S[:,cg*i+0] =  muu*T1[i,:] + muu*T2[i,:] + N[i,:];
                        S[:,cg*i+1] =  muu*T1[i,:] - muu*T2[i,:] + N[i,:];
                        S[:,cg*i+2] = -muu*T1[i,:] + muu*T2[i,:] + N[i,:];
                        S[:,cg*i+3] = -muu*T1[i,:] - muu*T2[i,:] + N[i,:];
                else:
                        S[:,cg*i+0] =   mu*T1[i,:] + N[i,:];
                        S[:,cg*i+1] =  -mu*T1[i,:] + N[i,:];
                        S[:,cg*i+2] =   mu*T2[i,:] + N[i,:];
                        S[:,cg*i+3] = - mu*T2[i,:] + N[i,:];
                
                S[:,cg*i+0] = S[:,cg*i+0]/np.linalg.norm(S[:,cg*i+0]);
                S[:,cg*i+1] = S[:,cg*i+1]/np.linalg.norm(S[:,cg*i+1]);
                S[:,cg*i+2] = S[:,cg*i+2]/np.linalg.norm(S[:,cg*i+2]);
                S[:,cg*i+3] = S[:,cg*i+3]/np.linalg.norm(S[:,cg*i+3]);

        ''' compute matrix mapping contact forces to gravito-inertial wrench '''
        M = np.zeros((6,3*c));
        for i in range(c):
                M[:3, 3*i:3*i+3] = np.identity(3);
                M[3:, 3*i:3*i+3] = crossMatrix(p[i,:]);
                
        ''' project generators in 6d centroidal space '''
        S_centr = np.zeros((6,m));
        for i in range(c):
                S_centr[:,cg*i:cg*i+cg] = np.dot(M[:,3*i:3*i+3], S[:,cg*i:cg*i+cg]);
        return S_centr

#~ /* Compute the robustness measure of the equilibrium of a specified CoM position
     #~ * by solving the following LP:
          #~ find          b, b0
          #~ minimize      -b0
          #~ subject to    D c + d <= G b    <= D c + d
                        #~ 0       <= b - b0 <= Inf
        #~ where
          #~ b         are the coefficient of the contact force generators (f = V b)
          #~ b0        is the robustness measure
          #~ c         is the CoM position
          #~ G         is the matrix whose columns are the gravito-inertial wrench generators
    #~ */
def robust_eq(P, N, mass, nu, com, ddc, g=array([0,0,-9.81]), dL =array([0,0,0]) ):
        m = P.shape[0] * cg
        C = zeros((6,m+1))
        C[:6,:m] = compute_generators(P, N, mass, g, nu)
        d =  zeros(6);
        d[:3]  = mass * (ddc - g)
        d[3:]  = cross(com,mass * (ddc - g))

        G = zeros((m,m+1))
        G[:,:-1] = identity(m)
        G[:,-1] = -ones(m)
        h = zeros(m)
        
        q = zeros(m+1)
        q[-1] = -1
        
        return solve_lp(q, G=-G, h=-h, C=C, d=d)

from math import exp, log
from numpy import array, dot, vstack, hstack, asmatrix, identity, sqrt

from numpy import linalg as la


def isPD(B):
    """Returns true when input is positive-definite, via Cholesky"""
    try:
        _ = la.cholesky(B)
        return True
    except la.LinAlgError:
        return False

def nearestPD(A):
    """Find the nearest positive-definite matrix to input

    A Python/Numpy port of John D'Errico's `nearestSPD` MATLAB code [1], which
    credits [2].

    [1] https://www.mathworks.com/matlabcentral/fileexchange/42885-nearestspd

    [2] N.J. Higham, "Computing a nearest symmetric positive semidefinite
    matrix" (1988): https://doi.org/10.1016/0024-3795(88)90223-6
    """

    B = (A + A.T) / 2
    _, s, V = la.svd(B)

    H = np.dot(V.T, np.dot(np.diag(s), V))

    A2 = (B + H) / 2

    A3 = (A2 + A2.T) / 2

    if isPD(A3):
        return A3

    spacing = np.spacing(la.norm(A))
    # The above is different from [1]. It appears that MATLAB's `chol` Cholesky
    # decomposition will accept matrixes with exactly 0-eigenvalue, whereas
    # Numpy's will not. So where [1] uses `eps(mineig)` (where `eps` is Matlab
    # for `np.spacing`), we use the above definition. CAVEAT: our `spacing`
    # will be much larger than [1]'s `eps(mineig)`, since `mineig` is usually on
    # the order of 1e-16, and `eps(1e-16)` is on the order of 1e-34, whereas
    # `spacing` will, for Gaussian random matrixes of small dimension, be on
    # othe order of 1e-16. In practice, both ways converge, as the unit test
    # below suggests.
    I = np.eye(A.shape[0])
    k = 1
    while not isPD(A3):
        mineig = np.min(np.real(la.eigvals(A3)))
        A3 += I * (-mineig * k**2 + spacing)
        k += 1

    return A3


def min_acc_error(P, N, mass, nu, com, ddc, g=array([0,0,-9.81]), dL =array([0,0,0])):
        G  = compute_generators(P, N, mass, g, nu)
        H = zeros((6,3)); H[:3,:3] = identity(3);
        H[3:,:] = crossMatrix(com); H*=mass;
        h = zeros(6); h[:3] = -g * mass; h[3:] =  cross(com,h[:3])        
        
        C =  G[:]
        d =  H.dot(ddc) +  h
        
        m = P.shape[0] * cg 
        G = -identity(m)        
        h = zeros(m)
        
        #rather than a qp, try to minimize error between equality to always have a solution
        
        w = 1
        P = dot(C.T, C)       
        #~ P = nearestPD(w*2*P) + np.eye(P.shape[0]) * (0.0000000000001)
        P = 2*w*P + np.eye(P.shape[0]) * (0.000000001)
        #~ print "nearestPD1", isPD(nearestPD(dot(C.T, C)))
        #~ print "nearestPD", isPD(P)
        q_qp = -w *2*dot(d, C)
        try:
                x_start = quadprog_solve_qp(P, q_qp, G, h)
                res =  np.linalg.norm(C.dot(x_start) - d  )
                if(res < 1.e-8):
                        return 0
                else:
                        return res
        except ValueError as e:
                print "m", m
                print 'G', C
                print e
                return 4000
        
                #~ return 10e15

# from pierre's paper
# find maximum acceleration over current direction. Use it as robustness of the motion
# contrary to paper, alpha is not constrained to be positive, to have a idea of 
# the failure
def extremum_overline(P, N, mass, nu, com, ddc, g=array([0,0,-9.81]), dL =array([0,0,0])):
        G  = compute_generators(P, N, mass, g, nu)
        H = zeros((6,3)); H[:3,:3] = identity(3);
        H[3:,:] = crossMatrix(com); H*=mass;
        h = zeros(6); h[:3] = -g * mass; h[3:] =  cross(com,h[:3])
        alpha_req = norm(ddc)
        a = ddc
        if alpha_req == 0.:
                a = array([1.,0.,0.])
        if alpha_req > 0:
                a = ddc / alpha_req
        
        m = P.shape[0] * cg
        
        C = zeros((6,m+1))
        C[:,:m] = G
        C[:,-1] = -H.dot(a)
        d =  h[:]
        
        G = zeros((m,m+1))
        G[:,:-1] = -identity(m)        
        h = zeros(m)
        
        q = zeros(m+1)
        q[-1] = -1
        
        r = solve_lp(q, G=G, h=h, C=C, d=d)
        #~ try:
                #~ return (alpha_req - r[-1])**3
        #~ except OverflowError:
                #~ return float('inf')
        
        if r[-1] > alpha_req:
                return 0
        else:
                return alpha_req - r[-1]
                #~ return 10e15

if __name__ == '__main__':
        z = array([0.,0.,1.])
        P = array([array([x,y,0]) for x in [-0.05,0.05] for y in [-0.1,0.1]])
        N = array([z for _ in range(4)])
        mass = 54.
        nu = 0.3
        g = array([0.,0.,-9.81])
        ddc = array([1,0,0.])
        dL =  array([0,0,0.])
        com =  array([0,0,1.])



        from centroidal_dynamics import *
        eq = Equilibrium("test", mass, 4, SolverLP.SOLVER_LP_QPOASES ) 
        eq.setNewContacts(asmatrix(P),asmatrix(N),nu,EquilibriumAlgorithm.EQUILIBRIUM_ALGORITHM_LP)
        status, robustness3 = eq.computeEquilibriumRobustness(asmatrix(com),asmatrix(ddc))


        def test(mu = 0.3):
                r  = robust_eq(P, N, mass, mu, com, ddc)[-1]
                r2 = extremum_overline(P, N, mass, mu, com, ddc)

                print "robust found ", r > 0 
                print "extremum found ", r2 > 0 

                print "robustness ", r
                print "extremeum ", r2
                return r2
                
        test()
