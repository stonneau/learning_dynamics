from numpy import array, asmatrix, matrix, newaxis
import numpy as np

import theano
import theano.tensor as T

import sys
sys.path.append('../motion/')

from Quaternions import Quaternions

from qp_giwc import extremum_overline, min_acc_error

def computeCOM(V):
    #make it proportional to joint length
    parents=array([-1,0,1,2,3,4,1,6,7,8,1,10,11,12,12,14,15,16,12,18,19,20]);
    lengths=array([
            2.40,7.15,7.49,2.36,2.37,7.43,7.50,2.41,
            2.04,2.05,1.75,1.76,2.90,4.98,3.48,0.71,
            2.73,5.24,3.44,0.62]);
    masstot = sum(lengths)
                  
    J = V[:,:-7].reshape((V.shape[0], len(parents), 3, V.shape[2]))  
    Jmid =  ((J[:,2:] - (J[:,2:] - J[:,parents[2:]]) / 2.) 
    * lengths[newaxis,:,newaxis,newaxis]) / masstot
    #~ com = (T.sum( Jmid, axis = 1) * 0.01).eval() #cm to meters
    com = (T.sum( Jmid, axis = 1)).eval() #cm to meters
    com[:,[1, 2],:] = com[:,[2,1],:]
    return com
    
def computeDDC(coms):
    dc = (coms[:,:,1:] - coms[:,:,:-1]) * 60 #fps
    return dc[:,:,1:] - dc[:,:,:-1]

def giwc(mass, P, N, nu):
    eq = Equilibrium("test", mass, 4)     
    z = array([0.,0.,1.])    
    eq.setNewContacts(asmatrix(P),asmatrix(N),nu,EquilibriumAlgorithm.EQUILIBRIUM_ALGORITHM_PP)
    H,h = eq.getPolytopeInequalities()
    return H
    
numfail = 0
eps = 0.00000000001

from centroidal_dynamics import *

eq = None

def robustness(mass, P, N, nu,c, ddc):
    try:
        return min_acc_error(P, N, mass, nu , c, ddc, g=array([0,0,-9.81]), dL =array([0,0,0]))
    except ValueError as e:
        print e
        return 4000
    
def squareContact(center, normal,side):
    rg = [-side,side]    
    return [[center[0]+rgi,center[1],center[2]+rgj] for rgi in rg for rgj in rg]
    
def equilibriumRobustnessForFrame(contactframe, frame, com, ddc, feet, mass, nu):
    z = array([0.,0.,1.])
    #~ cpoints = [frame[foot] * 0.01 for i, foot in enumerate(feet) if contactframe[i] > 0.5]    
    cpoints = [squareContact(frame[foot], z, 0.1)  for i, foot in enumerate(feet) if contactframe[i] > 0.5]    
    cpoints = [el for sublist in cpoints for el in sublist]
    if(len(cpoints) > 0):
        P = array(cpoints)
        P[:,[1, 2]] = P[:,[2,1]]        
        N = array([z for _ in range(len(cpoints))])
        return robustness(mass, P, N, nu,com, ddc)
    else:
        return 0
        
def dynamicDataForFrame(frame, com, ddc, feet, mass, nu):
    z = array([0.,0.,1.])
    cpoints = [np.array([frame[foot][0],frame[foot][2],frame[foot][1]])  for foot in feet]    
    return [ cpoints, com, ddc]
    
def computeContactPhases(contact, V, feet, mass, nu = 0.3):    
    phases = sorted(list(set((contact[:,:,1:] - contact[:,:,:-1]).nonzero()[2])))
    cones = [giwcFromFrame(contact[:,:,i][0], V[:,:,i][0], feet, mass, nu) for i in phases]
    
    
def integratepositions(V):
    #~ V = V.eval()
    #~ for ai in range(len(animations)):
    anim = np.swapaxes(V[0].copy(), 0, 1)
    
    joints, root_x, root_z, root_r = anim[:,:-7], anim[:,-7], anim[:,-6], anim[:,-5]
    joints = joints.reshape((len(joints), -1, 3))
    
    #~ print 'joints.shape', joints.shape
    
    rotation = Quaternions.id(1)
    offsets = []
    translation = np.array([[0,0,0]])
    
    #~ if not ignore_root:
    for i in range(len(joints)):
        joints[i,:,:] = rotation * joints[i]
        #~ V[:,:, i] = joints[i,:,:]
        joints[i,:,0] = joints[i,:,0] + translation[0,0]
        joints[i,:,2] = joints[i,:,2] + translation[0,2]
        V[:,:-7, i] = joints[i,:,:].flatten() 
        rotation = Quaternions.from_angle_axis(-root_r[i], np.array([0,1,0])) * rotation
        offsets.append(rotation * np.array([0,0,1]))
        translation = translation + rotation * np.array([root_x[i], 0, root_z[i]])
    V[0,:-7,:]*=0.1
    return V
    
def getContactLabels(V, feet, oldcontacts):
    #~ feet = np.array([[12,13,14], [15,16,17],[24,25,26], [27,28,29]])
    feetIdx  = V[0][feet]
    vels = (feetIdx[:,:,1:]-feetIdx[:,:,:-1])**2
    velfactor = np.array([0.05,0.05,0.05,0.05]).reshape((4,1)), 
    heightfactor = np.array([3.0, 2.0,3.0, 2.0]).reshape((4,1))
    
    x = vels[:,0,:]
    y = vels[:,1,:]
    z = vels[:,2,:]
    h = feetIdx[:,1,:-1]
    contacts = (((x+y+z) < velfactor) and (h < heightfactor))
    #~ print "diff", np.linalg.norm(contacts-oldcontacts[:,:,:-1])
    print "diff", feetIdx[:,:,10:20]
    print "diff", contacts[:,:,10:20]
    print "diff", oldcontacts[:,:,10:20]
    return contacts
    
def computeRobustnessPerFrame(contacts, Vwhole, frameId, window_size, feet, mass, nu = 0.3): 
    #~ V = V0.copy()
    Vwho = Vwhole.copy()
    Vwho = integratepositions(Vwho)  #V.shape (1, 73, 240)   
    #~ V = Vwho[:,:,frameId:V0.shape[-1]]
    coms = computeCOM(Vwho)[:,:,frameId:min(frameId+window_size+2,Vwho.shape[-1])]
    #~ print "com shape ", coms.shape
    #~ contact = contacts.copy()
    #~ contact = getContactLabels(Vwho, feet, contacts)
    V = Vwho[:,:,frameId:frameId+window_size]
    #~ print "frameId", frameId
    #~ print "Vwho.shape", Vwho.shape
    #~ print "V.shape", V.shape
    #~ import matplotlib.pyplot as plt
    #~ from mpl_toolkits.mplot3d import Axes3D
    #~ fig = plt.figure()
    #~ ax = fig.add_subplot(111, projection='3d')
    #~ ax.scatter([coms[0,0,i] for i in range(240)], [coms[0,1,i] for i in range(240)], [coms[0,2,i] for i in range(240)], zdir='z', s=20, c="b")
    #~ ax.scatter([V[0,15,i] * contact[:,:,i][0][1] for i in range(240)], [V[0,17,i] * contact[:,:,i][0][1] for i in range(240)], [V[0,16,i] * contact[:,:,i][0][1] for i in range(240)], zdir='z', s=20, c="r")
    #~ ax.scatter([V[0,27,i] *contact[:,:,i][0][3] for i in range(240)], [V[0,29,i] *contact[:,:,i][0][3] for i in range(240)], [V[0,28,i] *contact[:,:,i][0][3] for i in range(240)], zdir='z', s=20, c="g")
    ddcs = computeDDC(coms)
    #~ print "ddcom shape ", coms.shape
    #~ ax.scatter([ddcs[0,0,i] for i in range(237)], [ddcs[0,1,i] for i in range(237)], [ddcs[0,2,i] for i in range(237)], zdir='z', s=20, c="g")
    
    #~ print ddcs.mean(axis = 2)
    
    #~ plt.show()
    #~ robs = [equilibriumRobustnessForFrame(contact[:,:,i][0], V[:,:,i][0], coms[:,:,i][0], ddcs[:,:,i][0], feet, mass, nu) for i in range(0,ddcs.shape[-1])]
    robs = [equilibriumRobustnessForFrame(contacts[:,:,i][0], V[:,:,i][0], coms[:,:,i][0], ddcs[:,:,i][0], feet, mass, nu) for i in range(ddcs.shape[-1])]
    #~ print "cost one iter", sum(robs)
    return sum(robs)
    #~ return sum(robs)

def computeDynamicsDataPerFrame(Vwhole, frameId, window_size, feet, mass, nu = 0.3): 
    Vwho = Vwhole.copy()
    Vwho = integratepositions(Vwho)  #V.shape (1, 73, 240)   
    coms = computeCOM(Vwho)[:,:,frameId:min(frameId+window_size+2,Vwho.shape[-1])]
    V = Vwho[:,:,frameId:frameId+window_size]
    ddcs = computeDDC(coms)
    return [dynamicDataForFrame(V[:,:,i][0], coms[:,:,i][0], ddcs[:,:,i][0], feet, mass, nu) for i in range(ddcs.shape[-1])]
    
#~ def cost()
